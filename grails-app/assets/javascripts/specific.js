/**
 * Created by ali on 13/12/15.
 */
var pageSpecific = function () {
    console.log("Hello from Specific page!")
};

$(document).ready(function () {
    if ($(document.body).data('page') == ('specific')) {
        pageSpecific()
    }
});